from scipy import interpolate
from datetime import datetime
from tqdm import tqdm
import pandas as pd
import numpy as np


source = list(range(30))

source[0] = pd.read_csv('001_210419081912A2.csv')
source[1] = pd.read_csv('001_210419111131A.csv')
source[2] = pd.read_csv('001_210419113047A.csv')
source[3] = pd.read_csv('001_210419121419A.csv')

source[4] = pd.read_csv('009_210418174005B.csv')
source[5] = pd.read_csv('009_210418213837B.csv', encoding='cp932')
source[6] = pd.read_csv('009_210419200449B.csv', encoding='cp932')
source[7] = pd.read_csv('009_210419202817B.csv')
source[8] = pd.read_csv('009_210419214717B.csv')
source[9] = pd.read_csv('009_210424111118B.csv')
source[10] = pd.read_csv('009_210424112829B.csv')
source[11] = pd.read_csv('009_210428185647B (2).csv')
source[12] = pd.read_csv('009_210428210343B.csv', encoding='cp932')

source[13] = pd.read_csv('012_210419071806A.csv')
source[14] = pd.read_csv('012_210419081939A2.csv')
source[15] = pd.read_csv('012_210419121429A.csv')

source[16] = pd.read_csv('210724_dataAI (2).csv')
source[17] = pd.read_csv('210724_dataAI.csv')
source[18] = pd.read_csv('210725_dataAI.csv')
source[19] = pd.read_csv('210726_dataAI (3).csv')
source[20] = pd.read_csv('210726_dataAI.csv')
source[21] = pd.read_csv('210727_dataAI.csv')
source[22] = pd.read_csv('210728_dataAI.csv')
source[23] = pd.read_csv('n210729_dataAI.csv')
source[24] = pd.read_csv('n210731_dataAI.csv')
source[25] = pd.read_csv('n210801_dataAI.csv')

source[26] = pd.read_csv('n18205936_0.csv')
source[27] = pd.read_csv('n19112444_0.csv')
source[28] = pd.read_csv('n19130927_0.csv')
source[29] = pd.read_csv('24125619_0.csv')

COLS = ['Time', 'Accel X', 'Accel Y', 'Accel Z',
        'Gyro X', 'Gyro Y', 'Gyro Z', '書く', '集中', '楽しさ']
comp_data_all = pd.DataFrame(columns=COLS)
single_data_all = pd.DataFrame(columns=COLS)
multi_data_all = pd.DataFrame(columns=COLS)
irregular_data_all = pd.DataFrame(columns=COLS)

for data_idx in range(30):

    data = source[data_idx]
    print('{}/{}'.format(data_idx, len(source)))
    tmp = list(data.Time)
    times = sorted(set(tmp), key=tmp.index)
    times = [e for e in times if e is not np.nan]

    comp_data = pd.DataFrame(columns=COLS)
    single_data = pd.DataFrame(columns=COLS)
    multi_data = pd.DataFrame(columns=COLS)
    irregular_data = pd.DataFrame(columns=COLS)
    irregular_len = []

    # A) データ前処理
    print('A) データ前処理')
    for time in tqdm(times):
        sample = data[data.Time == time]
        sample = sample[COLS]

        if len(sample) == 1:     # 1秒間隔のデータ
            single_data = pd.concat([single_data, sample])

        elif len(sample) == 10:  # 0.1秒間隔のデータ
            for idx, row in enumerate(sample.index):
                # データを直す
                sample.at[row, 'Time'] = time + '.' + str(idx)
            multi_data = pd.concat([multi_data, sample])

        else:                    # 不規則なデータ
            irregular_data = pd.concat([irregular_data, sample])
            irregular_len.append(len(sample))

    # B) 1秒間隔のデータの処理(single_data)
    print('B) 1秒間隔のデータの処理')
    single_data['unixtime'] = 0.0
    irregular_date_formats = []

    if len(single_data) > 0:
        # unixtime列を追加する
        print('B-1) unixtime列追加')
        df_dicts = single_data.to_dict(orient='index')
        for row, item in tqdm(df_dicts.items()):
            try:
                dt = datetime.strptime(item['Time'], '%Y/%m/%d %H:%M:%S')
                single_data.at[row, 'unixtime'] = dt.timestamp()
            except:
                irregular_date_formats.append(item['Time'])

        single_data = single_data[~single_data.Time.isin(
            irregular_date_formats)]
        single_data = single_data.sort_values(
            'unixtime').reset_index(drop=True)

        # 線形補完する(Complimentation)
        print('B-2) 線形補完')
        if len(single_data) > 0:
            method = interpolate.interp1d
            labels = ['Accel X', 'Accel Y', 'Accel Z',
                      'Gyro X', 'Gyro Y', 'Gyro Z']
            fitted_curve = dict()

            for label in tqdm(labels):
                x = list(single_data['unixtime'])
                y = list(single_data[label])
                fitted_curve[label] = method(x, y)

        # 0.1秒間隔にデータを補完してcomp_dataに格納する
        print('B-3) データ格納')
        if len(single_data) > 0:
            comp_data['unixtime'] = 0.0
            prev = 0
            df_dicts = single_data.to_dict(orient='index')
            for row, item in tqdm(df_dicts.items()):
                if item['unixtime'] - prev == 1:
                    for idx in range(10):
                        current = prev + idx / 10
                        comp_data = comp_data.append({'Time': item['Time'] + '.' + str(idx),
                                                      'Accel X': fitted_curve['Accel X'](current),
                                                      'Accel Y': fitted_curve['Accel Y'](current),
                                                      'Accel Z': fitted_curve['Accel Z'](current),
                                                      'Gyro X': fitted_curve['Gyro X'](current),
                                                      'Gyro Y': fitted_curve['Gyro Y'](current),
                                                      'Gyro Z': fitted_curve['Gyro Z'](current),
                                                      '書く': item['書く'],
                                                      '集中': item['集中'],
                                                      '楽しさ': item['楽しさ'],
                                                      'unixtime': current}, ignore_index=True)
                prev = item['unixtime']

            comp_data = comp_data.sort_values(
                'unixtime').reset_index(drop=True)

    # C) 0.1秒間隔のデータの処理(multi_data)
    print('C) 0.1秒間隔のデータの処理')
    multi_data['unixtime'] = 0.0
    irregular_date_formats = []

    if len(multi_data) > 0:
        # unixtime列を追加する
        df_dicts = multi_data.to_dict(orient='index')
        for row, item in tqdm(df_dicts.items()):
            try:
                dt = datetime.strptime(item['Time'], '%Y/%m/%d %H:%M:%S.%f')
                multi_data.at[row, 'unixtime'] = dt.timestamp()
            except:
                irregular_date_formats.append(item['Time'])

        multi_data = multi_data[~multi_data.Time.isin(irregular_date_formats)]
        multi_data = multi_data.sort_values('unixtime').reset_index(drop=True)

    # D) CSV出力
    print('D) CSV出力')
    comp_data.to_csv('single_data_{}.csv'.format(data_idx))
    multi_data.to_csv('multi_data_{}.csv'.format(data_idx))
    irregular_data.to_csv('irregular_data_{}.csv'.format(data_idx))

    print('1秒間隔のデータ数:{} -> 補完後データ数:{} | 0.1秒間隔のデータ数:{} | 不規則データ数:{}'.format(
        len(single_data), len(comp_data), len(multi_data), len(irregular_data)))
    print('不規則データ内訳')
    print(irregular_len)
