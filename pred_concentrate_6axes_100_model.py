import math
import pandas as pd
import numpy as np
import seaborn as sns
from tqdm import tqdm
from pickle import load

import torch
import torch.nn as nn
import torch.nn.functional as F

from sklearn import preprocessing
from sklearn.metrics import accuracy_score, f1_score, confusion_matrix


class MyDataSet(torch.utils.data.Dataset):
    def __init__(self, x, y, z, a, b, c, target, idx):

        self.x = x
        self.y = y
        self.z = z
        self.a = a
        self.b = b
        self.c = c
        self.target = target
        self.idx = idx
        self.length = len(x)

    def __len__(self):
        return self.length

    def __getitem__(self, index):

        if index < 10:
            index = 10

        _x = np.empty(0)
        _y = np.empty(0)
        _z = np.empty(0)
        _a = np.empty(0)
        _b = np.empty(0)
        _c = np.empty(0)
        for i in range(0, 10):
            _x = np.concatenate([_x, self.x[index-i]])
            _y = np.concatenate([_y, self.y[index-i]])
            _z = np.concatenate([_z, self.z[index-i]])
            _a = np.concatenate([_a, self.a[index-i]])
            _b = np.concatenate([_b, self.b[index-i]])
            _c = np.concatenate([_c, self.c[index-i]])

        x = _x.reshape(1, 100).astype('float32')
        y = _y.reshape(1, 100).astype('float32')
        z = _z.reshape(1, 100).astype('float32')
        a = _a.reshape(1, 100).astype('float32')
        b = _b.reshape(1, 100).astype('float32')
        c = _c.reshape(1, 100).astype('float32')
        target = self.target[index]
        idx = self.idx[index].reshape(1, 10)

        return x, y, z, a, b, c, target, idx


class PositionalEncoding(nn.Module):

    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(
            0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)


class Model(nn.Module):

    def __init__(self):
        super(Model, self).__init__()

        self.pos_encoder1 = PositionalEncoding(100, 0.3)
        encoder1 = nn.TransformerEncoderLayer(100, 10, dim_feedforward=10)
        self.encoder1 = nn.TransformerEncoder(encoder1, 2)
        self.linear1 = nn.Linear(100, 10)

        encoder = nn.TransformerEncoderLayer(10, 5, dim_feedforward=5)
        self.encoder = nn.TransformerEncoder(encoder, 2)
        self.linear = nn.Linear(60, 2)

    def forward(self, x, y, z, a, b, c):
        batch_size = x.shape[0]

        x = x.permute(1, 0, 2)
        x = self.encoder1(x)
        x = x.permute(1, 0, 2)
        x = self.pos_encoder1(x)
        x = self.linear1(x)

        y = y.permute(1, 0, 2)
        y = self.encoder1(y)
        y = y.permute(1, 0, 2)
        y = self.pos_encoder1(y)
        y = self.linear1(y)

        z = z.permute(1, 0, 2)
        z = self.encoder1(z)
        z = z.permute(1, 0, 2)
        z = self.pos_encoder1(z)
        z = self.linear1(z)

        a = a.permute(1, 0, 2)
        a = self.encoder1(a)
        a = a.permute(1, 0, 2)
        a = self.pos_encoder1(a)
        a = self.linear1(a)

        b = b.permute(1, 0, 2)
        b = self.encoder1(b)
        b = b.permute(1, 0, 2)
        b = self.pos_encoder1(b)
        b = self.linear1(b)

        c = c.permute(1, 0, 2)
        c = self.encoder1(c)
        c = c.permute(1, 0, 2)
        c = self.pos_encoder1(c)
        c = self.linear1(c)

        x = torch.cat([x, y, z, a, b, c], dim=1)

        x = x.permute(1, 0, 2)
        x = self.encoder(x)
        x = x.permute(1, 0, 2)
        x = x.reshape(batch_size, -1)
        x = F.relu((self.linear(x)))

        out = torch.log_softmax(x, dim=1)

        return out


data = list()
for i in range(30):
    data.append(pd.read_csv('nAIdata_20210811/single_data_{}.csv'.format(i)))
    data.append(pd.read_csv('nAIdata_20210811/multi_data_{}.csv'.format(i)))
data = pd.concat(data)
data = data.drop(columns=data.columns[0])
data = data.reset_index()

# 学習データとラベルに分ける
X = data[['Accel X', 'Accel Y', 'Accel Z', 'Gyro X', 'Gyro Y', 'Gyro Z']].values
y = data['集中'].values

X_0_idx_lst = list()
X_1_idx_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_idx_lst.append(i)
    elif y[i] == 1:
        X_1_idx_lst.append(i)

X_0_accx_lst = list()
X_1_accx_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_accx_lst.append(X[i, 0])
    elif y[i] == 1:
        X_1_accx_lst.append(X[i, 0])

X_0_accy_lst = list()
X_1_accy_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_accy_lst.append(X[i, 1])
    elif y[i] == 1:
        X_1_accy_lst.append(X[i, 1])

X_0_accz_lst = list()
X_1_accz_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_accz_lst.append(X[i, 2])
    elif y[i] == 1:
        X_1_accz_lst.append(X[i, 2])

X_0_anglex_lst = list()
X_1_anglex_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_anglex_lst.append(X[i, 3])
    elif y[i] == 1:
        X_1_anglex_lst.append(X[i, 3])

X_0_angley_lst = list()
X_1_angley_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_angley_lst.append(X[i, 4])
    elif y[i] == 1:
        X_1_angley_lst.append(X[i, 4])

X_0_anglez_lst = list()
X_1_anglez_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_anglez_lst.append(X[i, 5])
    elif y[i] == 1:
        X_1_anglez_lst.append(X[i, 5])


X0_len = len(X_0_accx_lst)
X1_len = len(X_1_accx_lst)

# 学習データ
X_0_idx_arr = np.array(X_0_idx_lst).reshape(int(X0_len/10), -1)  # id
X_0_accx_arr = np.array(X_0_accx_lst).reshape(int(X0_len/10), -1)
X_0_accy_arr = np.array(X_0_accy_lst).reshape(int(X0_len/10), -1)
X_0_accz_arr = np.array(X_0_accz_lst).reshape(int(X0_len/10), -1)
X_0_anglex_arr = np.array(X_0_anglex_lst).reshape(int(X0_len/10), -1)
X_0_angley_arr = np.array(X_0_angley_lst).reshape(int(X0_len/10), -1)
X_0_anglez_arr = np.array(X_0_anglez_lst).reshape(int(X0_len/10), -1)

X_1_idx_arr = np.array(X_1_idx_lst).reshape(int(X1_len/10), -1)  # id
X_1_accx_arr = np.array(X_1_accx_lst).reshape(int(X1_len/10), -1)
X_1_accy_arr = np.array(X_1_accy_lst).reshape(int(X1_len/10), -1)
X_1_accz_arr = np.array(X_1_accz_lst).reshape(int(X1_len/10), -1)
X_1_anglex_arr = np.array(X_1_anglex_lst).reshape(int(X1_len/10), -1)
X_1_angley_arr = np.array(X_1_angley_lst).reshape(int(X1_len/10), -1)
X_1_anglez_arr = np.array(X_1_anglez_lst).reshape(int(X1_len/10), -1)

X_idx = np.concatenate([X_0_idx_arr, X_1_idx_arr])  # id
X_accx = np.concatenate([X_0_accx_arr, X_1_accx_arr])
X_accy = np.concatenate([X_0_accy_arr, X_1_accy_arr])
X_accz = np.concatenate([X_0_accz_arr, X_1_accz_arr])
X_anglex = np.concatenate([X_0_anglex_arr, X_1_anglex_arr])
X_angley = np.concatenate([X_0_angley_arr, X_1_angley_arr])
X_anglez = np.concatenate([X_0_anglez_arr, X_1_anglez_arr])

# 教師データ
y = [0]*int(X0_len/10) + [1]*int(X1_len/10)

# 正規化
sscalera = load(open('model/concentrate/sscalera.pkl', 'rb'))
sscalerb = load(open('model/concentrate/sscalerb.pkl', 'rb'))
sscalerc = load(open('model/concentrate/sscalerc.pkl', 'rb'))
sscalerd = load(open('model/concentrate/sscalerd.pkl', 'rb'))
sscalere = load(open('model/concentrate/sscalere.pkl', 'rb'))
sscalerf = load(open('model/concentrate/sscalerf.pkl', 'rb'))


# get accx transform model ##################################################
print("sscalera.mean_", sscalera.mean_)
print("sscalera.scale_", sscalera.scale_)
mean_accx = torch.zeros(1, 10)
std_accx = torch.zeros(1, 10)
for i in range(10):
    mean_accx[0][i] = sscalera.mean_[i]
    std_accx[0][i] = sscalera.scale_[i]
print(mean_accx)
print(std_accx)
def transform_accx(t):
    t -= mean_accx
    t /= std_accx
    return t
samplex = torch.zeros(10, 10)
transform_accx_trace = torch.jit.trace(transform_accx, samplex)
transform_accx_trace.save("transform_concentrate_accx_model_20210928.pt")

# get accy transform model ##################################################
mean_accy = torch.zeros(1, 10)
std_accy = torch.zeros(1, 10)
print(sscalerb.mean_)
print(sscalerb.scale_)
for i in range(10):
    mean_accy[0][i] = sscalerb.mean_[i]
    std_accy[0][i] = sscalerb.scale_[i]
print(mean_accy)
print(std_accy)
def transform_accy(t):
    t -= mean_accy
    t /= std_accy
    return t
sampley = torch.zeros(10, 10)
transform_accy_trace = torch.jit.trace(transform_accy, sampley)
transform_accy_trace.save("transform_concentrate_accy_model_20210928.pt")

# get accz transform model ##################################################
mean_accz = torch.zeros(1, 10)
std_accz = torch.zeros(1, 10)
print(sscalerc.mean_)
print(sscalerc.scale_)
for i in range(10):
    mean_accz[0][i] = sscalerc.mean_[i]
    std_accz[0][i] = sscalerc.scale_[i]
print(mean_accz)
print(std_accz)
def transform_accz(t):
    t -= mean_accz
    t /= std_accz
    return t
samplez = torch.zeros(10, 10)
transform_accz_trace = torch.jit.trace(transform_accz, samplez)
transform_accz_trace.save("transform_concentrate_accz_model_20210928.pt")

# get anglex transform model ##################################################
mean_anglex = torch.zeros(1, 10)
std_anglex = torch.zeros(1, 10)
print(sscalerd.mean_)
print(sscalerd.scale_)
for i in range(10):
    mean_anglex[0][i] = sscalerd.mean_[i]
    std_anglex[0][i] = sscalerd.scale_[i]
print(mean_anglex)
print(std_anglex)
def transform_anglex(t):
    t -= mean_anglex
    t /= std_anglex
    return t
samplexx = torch.zeros(10, 10)
transform_anglex_trace = torch.jit.trace(transform_anglex, samplexx)
transform_anglex_trace.save("transform_concentrate_anglex_model_20210928.pt")

# get angley transform model ##################################################
mean_angley = torch.zeros(1, 10)
std_angley = torch.zeros(1, 10)
print(sscalere.mean_)
print(sscalere.scale_)
for i in range(10):
    mean_angley[0][i] = sscalere.mean_[i]
    std_angley[0][i] = sscalere.scale_[i]
print(mean_angley)
print(std_angley)
def transform_angley(t):
    t -= mean_angley
    t /= std_angley
    return t
sampleyy = torch.zeros(10, 10)
transform_angley_trace = torch.jit.trace(transform_angley, sampleyy)
transform_angley_trace.save("transform_concentrate_angley_model_20210928.pt")

# get anglez transform model ##################################################
mean_anglez = torch.zeros(1, 10)
std_anglez = torch.zeros(1, 10)
print(sscalerf.mean_)
print(sscalerf.scale_)
for i in range(10):
    mean_anglez[0][i] = sscalerf.mean_[i]
    std_anglez[0][i] = sscalerf.scale_[i]
print(mean_anglez)
print(std_anglez)
def transform_anglez(t):
    t -= mean_anglez
    t /= std_anglez
    return t
samplezz = torch.zeros(10, 10)
transform_anglez_trace = torch.jit.trace(transform_anglez, samplezz)
transform_anglez_trace.save("transform_concentrate_anglez_model_20210928.pt")



X_accx = sscalera.transform(X_accx)
X_accy = sscalerb.transform(X_accy)
X_accz = sscalerc.transform(X_accz)
X_anglex = sscalerd.transform(X_anglex)
X_angley = sscalere.transform(X_angley)
X_anglez = sscalerf.transform(X_anglez)

model = Model()

model_path = 'model/model_concentrate_6axes_100.pth'
model.load_state_dict(torch.load(model_path))

testset = MyDataSet(X_accx, X_accy, X_accz, X_anglex,
                    X_angley, X_anglez, y, X_idx)

testloader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=False)

pred_lst = list()
data['prediction'] = 0
model.eval()

dummy_input0 = torch.zeros(1, 1, 100)
dummy_input1 = torch.zeros(1, 1, 100)
dummy_input2 = torch.zeros(1, 1, 100)
dummy_input3 = torch.zeros(1, 1, 100)
dummy_input4 = torch.zeros(1, 1, 100)
dummy_input5 = torch.zeros(1, 1, 100)
traced_script_module = torch.jit.trace(model, (dummy_input0, dummy_input1, dummy_input2, dummy_input3, dummy_input4, dummy_input5))
import torch.utils.mobile_optimizer as mobile_optimizer
optimized_traced_model = mobile_optimizer.optimize_for_mobile(traced_script_module)
optimized_traced_model.save("pred_concentrate_model_20210928.pt")




for e in tqdm(testloader):

    res = model(e[0], e[1], e[2], e[3], e[4], e[5])
    pred = np.argmax(np.exp(res.detach().numpy()))
    pred_lst.append(pred)

    for el in e[7].reshape(-1):
        data.at[int(el), 'prediction'] = int(pred)

print('accuracy:{}'.format(accuracy_score(y, pred_lst)))
print('f1_score:{}'.format(f1_score(y, pred_lst)))
data.to_csv('result/concentrate_6axes_pred_100.csv')

cmx = confusion_matrix(y, pred_lst)
cmd = sns.heatmap(cmx, annot=True, fmt="d", cmap="Greens")
cmd.set_title('Concentration. 6 Axes. Accuracy:{:.3f}'.format(
    accuracy_score(y, pred_lst)))
figure = cmd.get_figure()
figure.savefig("result/concentrate_6axes_pred_100.jpg")
