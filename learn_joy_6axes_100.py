import math
import pandas as pd
import numpy as np
import seaborn as sns
from tqdm import tqdm
from pickle import dump

import torch
import torch.nn.functional as F
from torch import nn
import adabound

from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, accuracy_score, f1_score


# seed固定
SEED = 210718
np.random.seed(SEED)
torch.manual_seed(SEED)

# データ読み込み
data = list()
for i in range(30):
    data.append(pd.read_csv('nAIdata_20210811/single_data_{}.csv'.format(i)))
    data.append(pd.read_csv('nAIdata_20210811/multi_data_{}.csv'.format(i)))
data = pd.concat(data)

# 学習データとラベルに分ける
X = data[['Accel X', 'Accel Y', 'Accel Z', 'Gyro X', 'Gyro Y', 'Gyro Z']].values
y = data['楽しさ'].values

X_0_accx_lst = list()
X_1_accx_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_accx_lst.append(X[i, 0])
    elif y[i] == 1:
        X_1_accx_lst.append(X[i, 0])

X_0_accy_lst = list()
X_1_accy_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_accy_lst.append(X[i, 1])
    elif y[i] == 1:
        X_1_accy_lst.append(X[i, 1])

X_0_accz_lst = list()
X_1_accz_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_accz_lst.append(X[i, 2])
    elif y[i] == 1:
        X_1_accz_lst.append(X[i, 2])

X_0_anglex_lst = list()
X_1_anglex_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_anglex_lst.append(X[i, 3])
    elif y[i] == 1:
        X_1_anglex_lst.append(X[i, 3])

X_0_angley_lst = list()
X_1_angley_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_angley_lst.append(X[i, 4])
    elif y[i] == 1:
        X_1_angley_lst.append(X[i, 4])

X_0_anglez_lst = list()
X_1_anglez_lst = list()
for i in range(0, len(X)):
    if y[i] == 0:
        X_0_anglez_lst.append(X[i, 5])
    elif y[i] == 1:
        X_1_anglez_lst.append(X[i, 5])

X0_len = len(X_0_accx_lst)
X1_len = len(X_1_accx_lst)
if X0_len < X1_len:
    q0 = X1_len // X0_len
    q1 = 1
else:
    q0 = 1
    q1 = X0_len // X1_len

# 学習データ
X_0_accx_arr = np.array(X_0_accx_lst*q0).reshape(int(X0_len/10)*q0, -1)
X_0_accy_arr = np.array(X_0_accy_lst*q0).reshape(int(X0_len/10)*q0, -1)
X_0_accz_arr = np.array(X_0_accz_lst*q0).reshape(int(X0_len/10)*q0, -1)
X_0_anglex_arr = np.array(X_0_anglex_lst*q0).reshape(int(X0_len/10)*q0, -1)
X_0_angley_arr = np.array(X_0_angley_lst*q0).reshape(int(X0_len/10)*q0, -1)
X_0_anglez_arr = np.array(X_0_anglez_lst*q0).reshape(int(X0_len/10)*q0, -1)

X_1_accx_arr = np.array(X_1_accx_lst*q1).reshape(int(X1_len/10)*q1, -1)
X_1_accy_arr = np.array(X_1_accy_lst*q1).reshape(int(X1_len/10)*q1, -1)
X_1_accz_arr = np.array(X_1_accz_lst*q1).reshape(int(X1_len/10)*q1, -1)
X_1_anglex_arr = np.array(X_1_anglex_lst*q1).reshape(int(X1_len/10)*q1, -1)
X_1_angley_arr = np.array(X_1_angley_lst*q1).reshape(int(X1_len/10)*q1, -1)
X_1_anglez_arr = np.array(X_1_anglez_lst*q1).reshape(int(X1_len/10)*q1, -1)

X_accx = np.concatenate([X_0_accx_arr, X_1_accx_arr])
X_accy = np.concatenate([X_0_accy_arr, X_1_accy_arr])
X_accz = np.concatenate([X_0_accz_arr, X_1_accz_arr])
X_anglex = np.concatenate([X_0_anglex_arr, X_1_anglex_arr])
X_angley = np.concatenate([X_0_angley_arr, X_1_angley_arr])
X_anglez = np.concatenate([X_0_anglez_arr, X_1_anglez_arr])

# 教師データ
y = [0]*int(X0_len/10)*q0 + [1]*int(X1_len/10)*q1

# 訓練データ・検証データ・テストデータ分割
X_accx_train, X_accx_test, X_accy_train, X_accy_test, X_accz_train, X_accz_test, \
    X_anglex_train, X_anglex_test, X_angley_train, X_angley_test, X_anglez_train, X_anglez_test, y_train, y_test = \
    train_test_split(X_accx, X_accy, X_accz, X_anglex,
                     X_angley, X_anglez, y, random_state=SEED)

X_accx_train, X_accx_val, X_accy_train, X_accy_val, X_accz_train, X_accz_val, \
    X_anglex_train, X_anglex_val, X_angley_train, X_angley_val, X_anglez_train, X_anglez_val, y_train, y_val = \
    train_test_split(X_accx_train, X_accy_train, X_accz_train,
                     X_anglex_train, X_angley_train, X_anglez_train, y_train, random_state=SEED)

# 正規化
sscalera = preprocessing.StandardScaler()
sscalerb = preprocessing.StandardScaler()
sscalerc = preprocessing.StandardScaler()
sscalerd = preprocessing.StandardScaler()
sscalere = preprocessing.StandardScaler()
sscalerf = preprocessing.StandardScaler()

X_accx_train = sscalera.fit_transform(X_accx_train)
X_accy_train = sscalerb.fit_transform(X_accy_train)
X_accz_train = sscalerc.fit_transform(X_accz_train)
X_anglex_train = sscalerd.fit_transform(X_anglex_train)
X_angley_train = sscalere.fit_transform(X_angley_train)
X_anglez_train = sscalerf.fit_transform(X_anglez_train)

X_accx_val = sscalera.transform(X_accx_val)
X_accy_val = sscalerb.transform(X_accy_val)
X_accz_val = sscalerc.transform(X_accz_val)
X_anglex_val = sscalerd.transform(X_anglex_val)
X_angley_val = sscalere.transform(X_angley_val)
X_anglez_val = sscalerf.transform(X_anglez_val)

X_accx_test = sscalera.transform(X_accx_test)
X_accy_test = sscalerb.transform(X_accy_test)
X_accz_test = sscalerc.transform(X_accz_test)
X_anglex_test = sscalerd.transform(X_anglex_test)
X_angley_test = sscalere.transform(X_angley_test)
X_anglez_test = sscalerf.transform(X_anglez_test)

# StandardScalerオブジェクトを保存
dump(sscalera, open('model/joy/sscalera.pkl', mode='wb'))
dump(sscalerb, open('model/joy/sscalerb.pkl', mode='wb'))
dump(sscalerc, open('model/joy/sscalerc.pkl', mode='wb'))
dump(sscalerd, open('model/joy/sscalerd.pkl', mode='wb'))
dump(sscalere, open('model/joy/sscalere.pkl', mode='wb'))
dump(sscalerf, open('model/joy/sscalerf.pkl', mode='wb'))


class EarlyStopping:
    """Early stops the training if validation loss doesn't improve after a given patience."""

    def __init__(self, patience=7, verbose=False):
        """
        Args:
            patience (int): How long to wait after last time validation loss improved.
                            Default: 7
            verbose (bool): If True, prints a message for each validation loss improvement.
                            Default: False
        """
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.val_loss_min = np.Inf

    def __call__(self, val_loss, model):

        score = -val_loss

        if self.best_score is None:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
        elif score < self.best_score:
            self.counter += 1
            print(
                f'EarlyStopping counter: {self.counter} out of {self.patience}')
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
            self.counter = 0

    def save_checkpoint(self, val_loss, model):
        '''Saves model when validation loss decrease.'''
        if self.verbose:
            print(
                f'Validation loss decreased ({self.val_loss_min:.6f} --> {val_loss:.6f}).  Saving model ...')
        torch.save(model.state_dict(), 'model/model_joy_6axes_100.pth')
        self.val_loss_min = val_loss


class PositionalEncoding(nn.Module):

    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(
            0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)


class Model(nn.Module):

    def __init__(self):
        super(Model, self).__init__()

        self.pos_encoder1 = PositionalEncoding(100, 0.3)
        encoder1 = nn.TransformerEncoderLayer(100, 10, dim_feedforward=10)
        self.encoder1 = nn.TransformerEncoder(encoder1, 2)
        self.linear1 = nn.Linear(100, 10)

        encoder = nn.TransformerEncoderLayer(10, 5, dim_feedforward=5)
        self.encoder = nn.TransformerEncoder(encoder, 2)
        self.linear = nn.Linear(60, 2)

    def forward(self, x, y, z, a, b, c):
        batch_size = x.shape[0]

        x = x.permute(1, 0, 2)
        x = self.encoder1(x)
        x = x.permute(1, 0, 2)
        x = self.pos_encoder1(x)
        x = self.linear1(x)

        y = y.permute(1, 0, 2)
        y = self.encoder1(y)
        y = y.permute(1, 0, 2)
        y = self.pos_encoder1(y)
        y = self.linear1(y)

        z = z.permute(1, 0, 2)
        z = self.encoder1(z)
        z = z.permute(1, 0, 2)
        z = self.pos_encoder1(z)
        z = self.linear1(z)

        a = a.permute(1, 0, 2)
        a = self.encoder1(a)
        a = a.permute(1, 0, 2)
        a = self.pos_encoder1(a)
        a = self.linear1(a)

        b = b.permute(1, 0, 2)
        b = self.encoder1(b)
        b = b.permute(1, 0, 2)
        b = self.pos_encoder1(b)
        b = self.linear1(b)

        c = c.permute(1, 0, 2)
        c = self.encoder1(c)
        c = c.permute(1, 0, 2)
        c = self.pos_encoder1(c)
        c = self.linear1(c)

        x = torch.cat([x, y, z, a, b, c], dim=1)

        x = x.permute(1, 0, 2)
        x = self.encoder(x)
        x = x.permute(1, 0, 2)
        x = x.reshape(batch_size, -1)
        x = F.relu((self.linear(x)))

        out = torch.log_softmax(x, dim=1)

        return out


model = Model()


class MyDataSet(torch.utils.data.Dataset):
    def __init__(self, x, y, z, a, b, c, target):

        self.x = x
        self.y = y
        self.z = z
        self.a = a
        self.b = b
        self.c = c
        self.target = target
        self.length = len(x)

    def __len__(self):
        return self.length

    def __getitem__(self, index):

        if index < 10:
            index = 10

        _x = np.empty(0)
        _y = np.empty(0)
        _z = np.empty(0)
        _a = np.empty(0)
        _b = np.empty(0)
        _c = np.empty(0)
        for i in range(0, 10):
            _x = np.concatenate([_x, self.x[index-i]])
            _y = np.concatenate([_y, self.y[index-i]])
            _z = np.concatenate([_z, self.z[index-i]])
            _a = np.concatenate([_a, self.a[index-i]])
            _b = np.concatenate([_b, self.b[index-i]])
            _c = np.concatenate([_c, self.c[index-i]])

        x = _x.reshape(1, 100).astype('float32')
        y = _y.reshape(1, 100).astype('float32')
        z = _z.reshape(1, 100).astype('float32')
        a = _a.reshape(1, 100).astype('float32')
        b = _b.reshape(1, 100).astype('float32')
        c = _c.reshape(1, 100).astype('float32')
        target = self.target[index]

        return x, y, z, a, b, c, target


trainset = MyDataSet(X_accx_train, X_accy_train, X_accz_train,
                     X_anglex_train, X_angley_train, X_anglez_train, y_train)
trainloader = torch.utils.data.DataLoader(
    trainset, batch_size=100, shuffle=True)
validset = MyDataSet(X_accx_val, X_accy_val, X_accz_val,
                     X_anglex_val, X_angley_val, X_anglez_val, y_val)
validloader = torch.utils.data.DataLoader(
    validset, batch_size=10, shuffle=True)

criterion = nn.NLLLoss()
# optimizer = torch.optim.Adam(model.parameters(), lr=0.0001)
optimizer = adabound.AdaBound(model.parameters(), lr=0.001, final_lr=0.1)
earlystopping = EarlyStopping(patience=10, verbose=True)

print('学習開始')
for epoch in range(1000):
    print(epoch)
    train_loss = 0
    model.train()
    for data in tqdm(trainloader):
        optimizer.zero_grad()

        output = model(data[0], data[1], data[2], data[3], data[4], data[5])
        target = data[6]

        loss = criterion(output, target)
        train_loss += loss.item()

        loss.backward()
        optimizer.step()

    train_loss = train_loss / len(trainloader)
    print(f"Train Loss: {train_loss}")

    valid_loss = 0
    model.eval()
    for data in tqdm(validloader):

        with torch.no_grad():
            output = model.forward(data[0], data[1], data[2],
                                   data[3], data[4], data[5])
            target = data[6]

        loss = criterion(output, target)
        valid_loss += loss.item()

    valid_loss = valid_loss / len(validloader)
    print(f"Valid Loss: {valid_loss}")
    earlystopping(valid_loss, model)
    if earlystopping.early_stop:
        print("Early stopping")
        break


model.load_state_dict(torch.load("model/model_joy_6axes_100.pth"))

testset = MyDataSet(X_accx_test, X_accy_test, X_accz_test,
                    X_anglex_test, X_angley_test, X_anglez_test, y_test)

testloader = torch.utils.data.DataLoader(testset, batch_size=1, shuffle=False)

fin_lst = list()

for data in tqdm(testloader):
    model.eval()

    res = model(data[0], data[1], data[2], data[3], data[4], data[5])
    index = np.exp(res.detach().numpy())

    fin_lst.append(np.argmax(index))

print('accuracy:{}'.format(accuracy_score(y_test, fin_lst)))
print('f1_score:{}'.format(f1_score(y_test, fin_lst)))

cmx = confusion_matrix(y_test, fin_lst)
cmd = sns.heatmap(cmx, annot=True, fmt="d", cmap="Greens")
cmd.set_title('Joy. 6 Axes. Accuracy:{:.3f}'.format(
    accuracy_score(y_test, fin_lst)))
figure = cmd.get_figure()
figure.savefig("result/joy_6axes_100.jpg")

torch.save(model.state_dict(), "model/model_joy_6axes_100.pth")
